"use strict";


const findIpButton = document.getElementById("findIpButton");
const resultElement = document.getElementById("result");
let dataDisplayed = false;

findIpButton.addEventListener("click", findIPAddress);

async function findIPAddress() {
  try {
    // Використати fetch для виконання AJAX-запиту
    const response = await fetch("https://api.ipify.org/?format=json");
    const ipData = await response.json();

    // Відправити другий AJAX-запит за фізичною адресою
    const addressResponse = await fetch(`http://ip-api.com/json/${ipData.ip}?fields=continent,country,regionName,city,district`);
    const addressData = await addressResponse.json();

    // Вивести або приховати отримані дані на сторінці
    if (dataDisplayed) {
      resultElement.innerHTML = "";
    } else {
      resultElement.innerHTML = `
        <p>IP Address: ${ipData.ip}</p>
        <p>Continent: ${addressData.continent}</p>
        <p>Country: ${addressData.country}</p>
        <p>Region: ${addressData.regionName}</p>
        <p>City: ${addressData.city}</p>
        <p>District: ${addressData.district}</p>
      `;
    }

    // Змінити значення флага
    dataDisplayed = !dataDisplayed;
  } catch (error) {
    console.error("Error fetching IP address:", error.message);
  }
}




